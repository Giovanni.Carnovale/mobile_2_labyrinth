using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupBehaviour : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private float rotationTime = 0.5f, hoverDuration = 2f, hoverHeight = 0.3f, randomScaleFactor = 0.3f;


    void Awake()
    {
        GetComponent<Renderer>().material.color = Random.ColorHSV(0f, 1f, 0.7f, 1f, 0.9f, 1f);
        transform.localScale = transform.localScale * (Random.Range(1 - randomScaleFactor, 1 + randomScaleFactor));

        StartCoroutine(Rotation());
        StartCoroutine(Hover());
    }

    // Update is called once per frame
    void Update()
    {
    }

    IEnumerator Rotation()
    {
        Vector3 startRotation = transform.rotation.eulerAngles;
        Vector3 endRotation = startRotation + 360.0f * Vector3.one;
        float t = 0.0f;
        while (true)
        {
            t += Time.deltaTime;

            Vector3 rotation = Vector3.Lerp(startRotation, endRotation, t/rotationTime);
            transform.rotation = Quaternion.Euler(rotation);
            if (t > rotationTime)
            {
                startRotation = transform.rotation.eulerAngles;
                endRotation = startRotation + 360.0f * Vector3.one;
                t = 0;
            }
            yield return null;
        }
    }

    IEnumerator Hover()
    {
        Vector3 startHeight = transform.position;
        Vector3 highest = transform.position + new Vector3(0, hoverHeight, 0);
        Vector3 lowest = transform.position - new Vector3(0, hoverHeight, 0);

        float t = 0.0f;
        while (true)
        {
            t += Time.deltaTime;
            float time = 0.5f * Mathf.Sin(6.28f*t/hoverDuration) + 0.5f;

            Vector3 height = Vector3.Lerp(lowest, highest, time);
            transform.position = height;


            if (t > hoverDuration)
            {
                t = 0;
            }
            yield return null;
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("Picked up!");
            StopAllCoroutines();
            transform.SetParent(collision.transform);
        }
    }
}
