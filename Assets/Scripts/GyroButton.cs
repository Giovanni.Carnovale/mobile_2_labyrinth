using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyroButton : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (!SystemInfo.supportsGyroscope)
            this.enabled = false;
    }
}
