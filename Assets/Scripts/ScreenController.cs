using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenController : MonoBehaviour
{
    [SerializeField, Range(1, 10)]
    private float boundsMultiplier = 2f, snapMultiplier = 1.5f;
    [SerializeField]
    PlayerController player;
    [SerializeField]
    RectTransform container;

    private Vector3 defaultPos;
    private Rect controllerBounds;
    private float boundsSize;
    private float distanceToSnap;
    private RectTransform rtransform;




    // Start is called before the first frame update
    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();

        rtransform = GetComponent<RectTransform>();
        defaultPos = rtransform.position;

        distanceToSnap = Mathf.Max(container.rect.width, container.rect.height) * snapMultiplier;
        boundsSize = distanceToSnap * boundsMultiplier;

        Vector2 origin = new Vector2(rtransform.position.x - boundsSize / 2, rtransform.position.y - boundsSize / 2);
        controllerBounds = new Rect(origin, new Vector2(boundsSize, boundsSize));
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
            foreach (Touch touch in Input.touches)
            {
                if (touch.phase == TouchPhase.Began || touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
                {
                    Vector3 touch3D = new Vector3(touch.position.x, touch.position.y, 0);
                    if (controllerBounds.Contains(new Vector2(touch3D.x, touch3D.y)))
                    {
                        if (Vector3.Distance(touch3D, defaultPos) >= distanceToSnap)
                        {
                            Debug.Log("snapping");
                            rtransform.position =  defaultPos + Vector3.ClampMagnitude(touch3D-defaultPos, distanceToSnap);
                        }
                        else
                            rtransform.position = touch3D;
                        float xInput = (rtransform.position.x - defaultPos.x) / distanceToSnap;
                        float yInput = (rtransform.position.y - defaultPos.y) / distanceToSnap;

                        player.controllerInput(xInput, yInput);
                    }
                }
                else
                {
                    rtransform.position = defaultPos;
                    player.endControllerInput();
                }
            }
        else
            rtransform.position = defaultPos;
    }
}
