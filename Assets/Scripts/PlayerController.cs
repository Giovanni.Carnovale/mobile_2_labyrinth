using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //General Options
    [SerializeField]
    private float speed = 10f, maxSpeed = 25f;

    //Touch parameters
    [SerializeField]
    private float decelerationMargin = 0.75f, targetMargin = 0.1f, maxForce = 20f,
        decelerationFactor = 0.5f;

    //Gyro parameters
    [SerializeField]
    private float gyroInitializationWait = 3f, deadzoneAngle = 10f, ignoreAngle = 1f, gyroForceCoefficient = 2f, maxGyroForce = 5f;

    //Virtual controller parameters
    [SerializeField]
    private float virtualControllerCoefficient = 3f;

    private Quaternion originRotation;

    //Controls input for gyro.
    private float xAxis, yAxis;
    //Controls input from virtual controller
    private float v_xAxis, v_yAxis;

    private const float RAY_MAX_DISTANCE = 100f;
    private const int LAYER_MASK = (1<<6);

    private Vector3 nextPos;
    private bool moving, gotControllerInput;

    private Gyroscope gyro;
    private Rigidbody rigidbody;

    // Start is called before the first frame update
    void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();

        moving = false;
        gotControllerInput = false;

        nextPos = Vector3.zero;

        xAxis = 0; yAxis = 0;

        
    }

    private void Start()
    {
        gyro = Input.gyro;
        gyro.enabled = false;
        StartCoroutine(gyroInitializer()); //Gyro may not be immediately ready but could start working after a while in some phones
    }

    // Update is called once per frame
    void Update()
    {

        if (!gotControllerInput)
        {
            if (Input.touchCount > 0)
            {
                foreach (Touch touch in Input.touches)
                {
                    if (touch.phase == TouchPhase.Began)
                    {
                        Ray ray = Camera.main.ScreenPointToRay(touch.position);
                        RaycastHit hit;

                        if (Physics.Raycast(ray, out hit, RAY_MAX_DISTANCE, LAYER_MASK))
                        {
                            Debug.DrawLine(ray.origin, hit.point, Color.red, 2f);
                            moving = true;
                            nextPos = hit.point;
                            nextPos.y = transform.position.y;
                        }
                    }
                }
            }
            else if (!moving && gyro.enabled)
            {
                Quaternion rotation = gyro.attitude;

                rotation = originRotation * GyroToUnity(gyro.attitude);
                rotation = Quaternion.Euler(0, 0, 90f) * rotation;

                xAxis = ClampAngle(rotation.eulerAngles.x);
                yAxis = ClampAngle(rotation.eulerAngles.y);
                
            }
            else
            {
                xAxis = 0;
                yAxis = 0;
            }
        }
    }


    private void FixedUpdate()
    {
        if (gotControllerInput)
        {
            Vector3 force = new Vector3(v_xAxis, 0, v_yAxis);
            force = virtualControllerCoefficient*Physics.gravity.magnitude * force.normalized;  //Gravity vector modulated in angle.
            force = Vector3.ClampMagnitude(force, maxForce);
            rigidbody.AddForce(force, ForceMode.Force);
        }
        else if (moving)
        {
            //Received touch
            float distanceFromTarget = Vector3.Distance(transform.position, nextPos);
            if (distanceFromTarget < decelerationMargin)
            {
                rigidbody.velocity *= decelerationFactor;
                rigidbody.angularVelocity *= decelerationFactor;
                if(distanceFromTarget < targetMargin || rigidbody.velocity.magnitude <= 0.1f)
                {
                    //snap and stop
                    rigidbody.velocity = Vector3.zero;
                    rigidbody.angularVelocity = Vector3.zero;
                    //rigidbody.position = nextPos;
                    moving = false;
                }
            }
            else if (moving)
            {
                Vector3 force_direction = nextPos - transform.position;
                force_direction = speed * force_direction;
                force_direction = Vector3.ClampMagnitude(force_direction, maxForce);
                rigidbody.AddForce(force_direction, ForceMode.Force);
            }
        } else
        { //Got input from gyroscope
            Vector3 force = new Vector3(xAxis, 0, yAxis); 
            force = gyroForceCoefficient * Physics.gravity.magnitude * force.normalized;  //Gravity vector modulated in angle.
            force = Vector3.ClampMagnitude(force, maxGyroForce);
            rigidbody.AddForce(force, ForceMode.Force);
        }

        if(rigidbody.velocity.magnitude > maxSpeed)
        {
            rigidbody.velocity = rigidbody.velocity.normalized * maxSpeed;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 3)//If I hit a wall, stop moving and respond to collision
            moving = false;
    }

    private float ClampAngle(float value)
    {
        //transforms from [0, 360] to [-180, 180] and checks for deadzone
        value = value <= 180 ? value : (value - 360);
        if(Mathf.Abs(value) < ignoreAngle) {
            return 0f;
        }
        if (Mathf.Abs(value) < deadzoneAngle)
            return deadzoneFunction(value);
        return value;
    }

    private float deadzoneFunction(float value)
    {
        return 1 / (deadzoneAngle * deadzoneAngle) * value * value * value; //Cubically slow
    }

    private static Quaternion GyroToUnity(Quaternion q)
    {
        //Unity and Gyroscope have different coordinate systems, this converts between them
        return new Quaternion(q.x, q.y, -q.z, -q.w);
    }

    private void setOriginRotation()
    {
        originRotation = gyro.enabled ? gyro.attitude : Quaternion.identity;
        originRotation = Quaternion.Inverse(GyroToUnity(originRotation));
    }

    private IEnumerator gyroInitializer()
    {
        //Gyroscope may not be immediately ready, wait for a bit before initializing it
        if (SystemInfo.supportsGyroscope)
            gyro.enabled = true;
        yield return new WaitUntil(()=>!gyro.attitude.Equals(new Quaternion(0, 0, 0, 0)));
        setOriginRotation();
    }

    public void restartGyro()
    {
        if (SystemInfo.supportsGyroscope)
            gyro.enabled = true;
        setOriginRotation();
    }


    /*
    private void OnGUI()
    {
        GUI.skin.label.fontSize = Screen.width / 40;
        GUILayout.Label("gyro status: " + SystemInfo.supportsGyroscope + " input: " + gyro.enabled);
        GUILayout.Label("Origin Rotation: " + originRotation.eulerAngles);
        GUILayout.Label("gyro.attitude: " + (originRotation * GyroToUnity(gyro.attitude)).eulerAngles);
    }
    */

    public void controllerInput(float xInput, float yInput)
    {
        moving = false;
        v_xAxis = speed * xInput;
        v_yAxis = speed * yInput;
        gotControllerInput = true;
    }

    public void endControllerInput()
    {
        v_xAxis = 0; v_yAxis = 0;
        gotControllerInput = false;
    }

    public void setMoving(bool value)
    {
        moving = value;
    }
}

